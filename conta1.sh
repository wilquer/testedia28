#!/bin/bash

if [ $# -ne 2 ]; then
echo "Uso: $0 <a> <b>"
    exit 1
fi

a=$1
b=$2

read -p "Digite a operação (+, -, *, /, **): " op

echo "$a $op $b" | bc

