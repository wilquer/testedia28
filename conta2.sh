#!/bin/bash

read -p "Digite o primeiro número: " num1
read -p "Digite o segundo número: " num2

resultado=$(echo "$num1 + $num2" | bc)

echo "O resultado da soma é: $resultado"
